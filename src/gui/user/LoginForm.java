/*
 * Created by JFormDesigner on Tue Jun 04 20:28:17 CEST 2019
 */

package gui.user;

import java.awt.event.*;

import client.chat.ChatClient;
import client.rest.RestClient;
import model.User;
import util.ExceptionLogger;

import java.awt.*;
import java.util.logging.Level;
import javax.swing.*;

/**
 * @author Nemanja Vrljanović
 */
public class LoginForm extends JFrame {

    private LoginForm() {
        initComponents();
    }

    private void login(ActionEvent e) {
        try {
            User user = new User(usernameTextField.getText(), new String(passwordField.getPassword()));
            if (RestClient.login((user))) {
                this.setVisible(false);
                usernameTextField.setText("");
                passwordField.setText("");
                ChatClient chatClient = new ChatClient(user);
                UserForm userForm = new UserForm(this, chatClient);
                userForm.setVisible(true);
            } else {
                usernameTextField.setText("");
                passwordField.setText("");
                JOptionPane.showMessageDialog(null, "Wrong Credentials");
            }
        } catch (Exception ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
            JOptionPane.showMessageDialog(this, "Server is not working. Try again later.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        usernameTextField = new JTextField();
        passwordField = new JPasswordField();
        loginButton = new JButton();
        label1 = new JLabel();
        label2 = new JLabel();

        //======== this ========
        setTitle("MDP App - Login");
        setName("loginFrame");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        var contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(usernameTextField);
        usernameTextField.setBounds(55, 65, 185, 35);
        contentPane.add(passwordField);
        passwordField.setBounds(55, 125, 185, 35);

        //---- loginButton ----
        loginButton.setText("Login");
        loginButton.setActionCommand("login");
        loginButton.addActionListener(e -> login(e));
        contentPane.add(loginButton);
        loginButton.setBounds(90, 190, 110, 35);

        //---- label1 ----
        label1.setText("Username");
        contentPane.add(label1);
        label1.setBounds(55, 50, 105, 13);

        //---- label2 ----
        label2.setText("Password");
        contentPane.add(label2);
        label2.setBounds(55, 110, 120, label2.getPreferredSize().height);

        contentPane.setPreferredSize(new Dimension(295, 280));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JTextField usernameTextField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JLabel label1;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    public static void main(String[] args) {
        new LoginForm().setVisible(true);
    }
}