/*
 * Created by JFormDesigner on Tue Jun 04 20:40:15 CEST 2019
 */

package gui.user;

import java.awt.event.*;
import javax.swing.table.*;

import client.chat.ChatClient;
import client.rest.RestClient;
import client.rmi.FileManagerClient;
import client.serialization.AnnouncementSerializer;
import util.ExceptionLogger;

import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * @author Nemanja Vrljanović
 */
class UserForm extends JFrame {

    private JFrame parentFrame;
    private ChatClient loggedUser;
    private boolean loggedIn;

    private static String ANNOUNCEMENT_MULTICAST_ADDRESS;
    private static int ANNOUNCEMENT_MULTICAST_PORT;

    static{
        Properties properties = new Properties();
        try {
            properties.load(new InputStreamReader(new FileInputStream("announcement.properties")));
            ANNOUNCEMENT_MULTICAST_ADDRESS = properties.getProperty("ANNOUNCEMENT_MULTICAST_ADDRESS");
            ANNOUNCEMENT_MULTICAST_PORT = Integer.parseInt(properties.getProperty("ANNOUNCEMENT_MULTICAST_PORT"));
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    UserForm(JFrame parentFrame, ChatClient user) {
        this.loggedUser = user;
        this.parentFrame = parentFrame;
        this.loggedIn = true;
        initComponents();
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                logout(null);
            }
        });

        Set<String> allUsers = RestClient.getAllUsers();
        allUsers.remove(loggedUser.getUser().getUsername());
        usersList.setListData(allUsers.toArray());
        listFiles();
        receiveAnnouncement();
        receiveMessages();
    }

    private void receiveMessages(){
        new Thread(()->{
            while(loggedIn){
                try {
                    String[] msgInfo = loggedUser.receiveMessage();
                    if (msgInfo != null) {
                        if (msgInfo.length == 3) {
                            groupChatTextArea.append(msgInfo[1] + ": " + msgInfo[2] + "\n");
                        } else {
                            String sender = msgInfo[0];
                            String content = msgInfo[1];
                            if (tabExist(sender)) {
                                JScrollPane pane = getScrollPane(sender);
                                ((JTextArea) pane.getViewport().getView()).append(sender + ": " + content + "\n");
                                chatPane.setSelectedComponent(pane);
                            } else {
                                JScrollPane pane = new JScrollPane();
                                JTextArea area = new JTextArea();
                                pane.setViewportView(area);
                                chatPane.addTab(sender, pane);
                                chatPane.setSelectedComponent(pane);
                                area.append(sender + ": " + content + "\n");
                            }
                        }
                        Thread.yield();
                    }
                }catch(IOException ex) {
                    ExceptionLogger.log(Level.SEVERE, ex.getMessage());
                }
            }
        }).start();
    }

    private void logout(MouseEvent e) {
        if(RestClient.logout(loggedUser.getUser())) {
            this.loggedIn = false;
            this.setVisible(false);
            this.parentFrame.setVisible(true);
            loggedUser.closeConnection();
        }
        else{
            JOptionPane.showMessageDialog(null, "UnexpectedError");
        }
    }

    private void changePassword(MouseEvent e) {
        new ChangePasswordForm(loggedUser.getUser()).setVisible(true);
    }

    private void showStatistics(MouseEvent e) {
        String statistics = RestClient.getStatistics(loggedUser.getUser());
        new StatisticsForm(this, statistics).setVisible(true);
    }

    private boolean tabExist(String title){
        for(int i = 0; i < chatPane.getTabCount(); ++i)
            if(chatPane.getTitleAt(i).equals(title))
                return true;
        return false;
    }

    private JScrollPane getScrollPane(String title){
        for(int i = 0; i < chatPane.getTabCount(); ++i){
            if(chatPane.getTitleAt(i).equals(title))
                return (JScrollPane)chatPane.getComponentAt(i);
        }
        return null;
    }

    private void sendMessageTo(MouseEvent e) {
        loggedUser.askForLoggedUsers();
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        String receiver = (String)usersList.getSelectedValue();
        if(receiver == null)
            return;
        Set<String> loggedUsers = loggedUser.getCurrentlyLoggedUsers();
        if(!loggedUsers.contains(receiver)){
            JOptionPane.showMessageDialog(this, "User is not active.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(!tabExist(receiver)) {
            JTextArea area = new JTextArea();
            area.setEditable(false);
            JScrollPane pane = new JScrollPane();
            pane.setViewportView(area);
            chatPane.addTab(receiver, pane);
            chatPane.setSelectedComponent(pane);
        }
        else
            chatPane.setSelectedComponent(getScrollPane(receiver));
    }

    private void listFiles(){
        new Thread(()-> {
            while(loggedIn) {
                try {
                    String[] files = FileManagerClient.getAllFiles(loggedUser.getUser().getUsername());
                    if (files == null)
                        return;
                    DefaultTableModel tableModel = (DefaultTableModel) filePreviewTable.getModel();
                    for (int i = tableModel.getRowCount() - 1; i >= 0 ; --i)
                        tableModel.removeRow(i);
                    Arrays.asList(files).forEach(e -> {
                        String sender = e.split("_")[0];
                        String filename = e.split("_")[1];
                        tableModel.addRow(new String[]{filename, sender});
                    });
                    Thread.sleep(5000);
                } catch (RemoteException | InterruptedException ex) {
                    ExceptionLogger.log(Level.SEVERE, ex.getMessage());
                }
            }
        }).start();
    }

    private void downloadFile(MouseEvent e) {
        int selectedRow = filePreviewTable.getSelectedRow();
        JFileChooser destChooser = new JFileChooser();
        destChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        destChooser.showSaveDialog(this);
        String destination = destChooser.getSelectedFile().getAbsolutePath();
        String filename = (String)filePreviewTable.getModel().getValueAt(selectedRow, 0);
        String sender = (String)filePreviewTable.getModel().getValueAt(selectedRow, 1);
        try {
            FileManagerClient.downloadFile(loggedUser.getUser().getUsername(), filename, sender, destination);
            JOptionPane.showMessageDialog(this, "File downloaded successfully", "Information", JOptionPane.INFORMATION_MESSAGE);
        }catch(IOException ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
            JOptionPane.showMessageDialog(this, "Could not download file", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void sendFile(MouseEvent e){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.showOpenDialog(this);
        String receiver = (String)usersList.getSelectedValue();
        if(receiver == null)
            return;
        try {
            FileManagerClient.sendFile(fileChooser.getSelectedFile(), loggedUser.getUser().getUsername(), receiver);
            JOptionPane.showMessageDialog(this, "File sent successfully", "Information", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
            JOptionPane.showMessageDialog(this, "Error while sending file", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void receiveAnnouncement(){
        new Thread(()-> {
            try {
                AnnouncementSerializer serializator = new AnnouncementSerializer();
                MulticastSocket multicastSocket = new MulticastSocket(ANNOUNCEMENT_MULTICAST_PORT);
                InetAddress address = InetAddress.getByName(ANNOUNCEMENT_MULTICAST_ADDRESS);
                multicastSocket.joinGroup(address);
                while (loggedIn) {
                    byte[] message = new byte[256];
                    DatagramPacket packet = new DatagramPacket(message, message.length);
                    multicastSocket.receive(packet);
                    String announcement = new String(message);
                    announcementLabel.setText("Last Announcement: " + announcement);
                    serializator.serializeAnnouncement(announcement);
                    Thread.sleep(200);
                }
            }catch (IOException | InterruptedException ex) {
                ExceptionLogger.log(Level.SEVERE, ex.getMessage());
            }
        }).start();
    }

    private void sendMessage(MouseEvent e) {
        loggedUser.askForLoggedUsers();
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        String content = messageField.getText();
        if("".equals(content))
            return;
        content = content.replace("#", "");
        JTextArea area = (JTextArea) ((JScrollPane) chatPane.getSelectedComponent()).getViewport().getView();
        int i = chatPane.getSelectedIndex();
        String to = chatPane.getTitleAt(i);
        if(! "Group Chat".equals(to) &&!loggedUser.getCurrentlyLoggedUsers().contains(to)){
            JOptionPane.showMessageDialog(this, "User is not active any more.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if("Group Chat".equals(to))
            loggedUser.sendGroupMessage(content);
        else
            loggedUser.sendMessage(to, content);
        area.append(loggedUser.getUser().getUsername() + ": " + content + " \n");
        messageField.setText("");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        announcementLabel = new JLabel();
        scrollPane1 = new JScrollPane();
        usersList = new JList();
        writeMessageButton = new JButton();
        sendMessageButton = new JButton();
        logoutButton = new JButton();
        userStatisticButton = new JButton();
        changePasswordButton = new JButton();
        scrollPane3 = new JScrollPane();
        filePreviewTable = new JTable();
        sendFileButton = new JButton();
        downloadFileButton = new JButton();
        label2 = new JLabel();
        label3 = new JLabel();
        label4 = new JLabel();
        messageField = new JTextField();
        chatPane = new JTabbedPane();
        chatScrollPane = new JScrollPane();
        groupChatTextArea = new JTextArea();

        //======== this ========
        setTitle("MDP Application");
        setResizable(false);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- announcementLabel ----
        announcementLabel.setText("Last Announcment: No Announcements yet");
        announcementLabel.setFont(announcementLabel.getFont().deriveFont(announcementLabel.getFont().getSize() + 2f));
        contentPane.add(announcementLabel);
        announcementLabel.setBounds(15, 55, 845, 23);

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(usersList);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(15, 105, 220, 250);

        //---- writeMessageButton ----
        writeMessageButton.setText("Write Message");
        writeMessageButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendMessageTo(e);
            }
        });
        contentPane.add(writeMessageButton);
        writeMessageButton.setBounds(120, 360, 116, 25);

        //---- sendMessageButton ----
        sendMessageButton.setText("Send Message");
        sendMessageButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendMessage(e);
            }
        });
        contentPane.add(sendMessageButton);
        sendMessageButton.setBounds(435, 360, 125, 25);

        //---- logoutButton ----
        logoutButton.setText("Logout");
        logoutButton.setFont(logoutButton.getFont().deriveFont(logoutButton.getFont().getSize() + 2f));
        logoutButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                logout(e);
            }
        });
        contentPane.add(logoutButton);
        logoutButton.setBounds(745, 10, 115, 30);

        //---- userStatisticButton ----
        userStatisticButton.setText("User Statistics");
        userStatisticButton.setFont(userStatisticButton.getFont().deriveFont(userStatisticButton.getFont().getSize() + 2f));
        userStatisticButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                showStatistics(e);
            }
        });
        contentPane.add(userStatisticButton);
        userStatisticButton.setBounds(15, 10, 150, 30);

        //---- changePasswordButton ----
        changePasswordButton.setText("Change Password");
        changePasswordButton.setFont(changePasswordButton.getFont().deriveFont(changePasswordButton.getFont().getSize() + 2f));
        changePasswordButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePassword(e);
            }
        });
        contentPane.add(changePasswordButton);
        changePasswordButton.setBounds(175, 10, 170, 30);

        //======== scrollPane3 ========
        {

            //---- filePreviewTable ----
            filePreviewTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            filePreviewTable.setAutoCreateRowSorter(true);
            filePreviewTable.setModel(new DefaultTableModel(
                new Object[][] {
                },
                new String[] {
                    "Filename", "Sender"
                }
            ) {
                Class<?>[] columnTypes = new Class<?>[] {
                    String.class, String.class
                };
                boolean[] columnEditable = new boolean[] {
                    false, false
                };
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                    return columnTypes[columnIndex];
                }
                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return columnEditable[columnIndex];
                }
            });
            {
                TableColumnModel cm = filePreviewTable.getColumnModel();
                cm.getColumn(0).setResizable(false);
                cm.getColumn(1).setResizable(false);
            }
            scrollPane3.setViewportView(filePreviewTable);
        }
        contentPane.add(scrollPane3);
        scrollPane3.setBounds(585, 105, 275, 250);

        //---- sendFileButton ----
        sendFileButton.setText("Send File");
        sendFileButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendFile(e);
            }
        });
        contentPane.add(sendFileButton);
        sendFileButton.setBounds(15, 360, 90, 25);

        //---- downloadFileButton ----
        downloadFileButton.setText("Download File");
        downloadFileButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                downloadFile(e);
            }
        });
        contentPane.add(downloadFileButton);
        downloadFileButton.setBounds(730, 360, 130, 25);

        //---- label2 ----
        label2.setText("Users");
        contentPane.add(label2);
        label2.setBounds(15, 90, 205, label2.getPreferredSize().height);

        //---- label3 ----
        label3.setText("Chat");
        contentPane.add(label3);
        label3.setBounds(255, 90, 290, label3.getPreferredSize().height);

        //---- label4 ----
        label4.setText("File Preview");
        contentPane.add(label4);
        label4.setBounds(585, 90, 265, label4.getPreferredSize().height);
        contentPane.add(messageField);
        messageField.setBounds(260, 330, 300, 25);

        //======== chatPane ========
        {

            //======== chatScrollPane ========
            {

                //---- groupChatTextArea ----
                groupChatTextArea.setEditable(false);
                groupChatTextArea.setLineWrap(true);
                chatScrollPane.setViewportView(groupChatTextArea);
            }
            chatPane.addTab("Group Chat", chatScrollPane);
        }
        contentPane.add(chatPane);
        chatPane.setBounds(260, 105, 300, 220);

        contentPane.setPreferredSize(new Dimension(880, 450));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JLabel announcementLabel;
    private JScrollPane scrollPane1;
    private JList usersList;
    private JButton writeMessageButton;
    private JButton sendMessageButton;
    private JButton logoutButton;
    private JButton userStatisticButton;
    private JButton changePasswordButton;
    private JScrollPane scrollPane3;
    private JTable filePreviewTable;
    private JButton sendFileButton;
    private JButton downloadFileButton;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JTextField messageField;
    private JTabbedPane chatPane;
    private JScrollPane chatScrollPane;
    private JTextArea groupChatTextArea;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}