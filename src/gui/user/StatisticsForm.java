/*
 * Created by JFormDesigner on Wed Jun 05 14:46:49 CEST 2019
 */

package gui.user;

import java.awt.*;
import javax.swing.*;

/**
 * @author Nemanja Vrljanović
 */
class StatisticsForm extends JDialog {

    StatisticsForm(Window owner, String statistics) {
        super(owner);
        setModal(true);
        initComponents();
        statisticsArea.setText(statistics);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        scrollPane1 = new JScrollPane();
        statisticsArea = new JTextArea();

        //======== this ========
        setTitle("Statistics");
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {

            //---- statisticsArea ----
            statisticsArea.setEditable(false);
            scrollPane1.setViewportView(statisticsArea);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(0, 0, 500, 320);

        contentPane.setPreferredSize(new Dimension(500, 350));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JScrollPane scrollPane1;
    private JTextArea statisticsArea;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}