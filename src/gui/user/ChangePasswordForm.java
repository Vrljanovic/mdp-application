/*
 * Created by JFormDesigner on Wed Jun 05 13:40:22 CEST 2019
 */

package gui.user;

import java.awt.event.*;

import client.rest.RestClient;
import model.User;

import java.awt.*;
import javax.swing.*;

/**
 * @author Nemanja Vrljanović
 */
class ChangePasswordForm extends JDialog {
    private User user;

    ChangePasswordForm(User user) {
        this.user = user;
        initComponents();
        setModal(true);
        this.usernameLabel.setText("Username: " + user.getUsername());
    }

    private void changePassword(MouseEvent e) {
        String oldPassword = new String(oldPasswordField.getPassword());
        String newPassword = new String(newPasswordField.getPassword());
        String repeatedPassword = new String(repeatPasswordField.getPassword());
        if(user.getPassword().equals(oldPassword)) {
            if (newPassword.equals(repeatedPassword)) {
                if (oldPassword.equals(repeatedPassword)) {
                    JOptionPane.showMessageDialog(null, "Your password can't be old password.", "Error", JOptionPane.ERROR_MESSAGE);
                    clearFields();
                } else {
                    user.setPassword(newPassword);
                    if (RestClient.changePassword(user)) {
                        JOptionPane.showMessageDialog(null, "Password changed successfully.", "Information", JOptionPane.INFORMATION_MESSAGE);
                        this.setVisible(false);
                    } else {
                        JOptionPane.showMessageDialog(null, "Error while changing password", "Error", JOptionPane.ERROR_MESSAGE);
                        user.setPassword(oldPassword);
                        this.setVisible(false);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Passwords don't match", "Error", JOptionPane.ERROR_MESSAGE);
                clearFields();
            }
        }else{
            JOptionPane.showMessageDialog(null, "You entered wrong password", "Error", JOptionPane.ERROR_MESSAGE);
            clearFields();
        }
    }

    private void clearFields(){
        this.oldPasswordField.setText("");
        this.newPasswordField.setText("");
        this.repeatPasswordField.setText("");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        usernameLabel = new JLabel();
        oldPasswordField = new JPasswordField();
        newPasswordField = new JPasswordField();
        repeatPasswordField = new JPasswordField();
        label2 = new JLabel();
        label3 = new JLabel();
        label4 = new JLabel();
        confirmButton = new JButton();

        //======== this ========
        setResizable(false);
        setTitle("Change Password");
        setType(Window.Type.POPUP);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- usernameLabel ----
        usernameLabel.setText("Username: ");
        usernameLabel.setFont(usernameLabel.getFont().deriveFont(usernameLabel.getFont().getSize() + 2f));
        contentPane.add(usernameLabel);
        usernameLabel.setBounds(20, 15, 155, 30);
        contentPane.add(oldPasswordField);
        oldPasswordField.setBounds(110, 65, 175, 25);
        contentPane.add(newPasswordField);
        newPasswordField.setBounds(110, 105, 175, 25);
        contentPane.add(repeatPasswordField);
        repeatPasswordField.setBounds(110, 145, 175, 25);

        //---- label2 ----
        label2.setText("Old Password");
        contentPane.add(label2);
        label2.setBounds(20, 65, 90, label2.getPreferredSize().height);

        //---- label3 ----
        label3.setText("New Password");
        contentPane.add(label3);
        label3.setBounds(20, 105, 85, label3.getPreferredSize().height);

        //---- label4 ----
        label4.setText("Repeat Password");
        contentPane.add(label4);
        label4.setBounds(20, 145, 90, label4.getPreferredSize().height);

        //---- confirmButton ----
        confirmButton.setText("Confirm");
        confirmButton.setFont(confirmButton.getFont().deriveFont(confirmButton.getFont().getSize() + 2f));
        confirmButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePassword(e);
            }
        });
        contentPane.add(confirmButton);
        confirmButton.setBounds(90, 200, 115, 30);

        contentPane.setPreferredSize(new Dimension(300, 300));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JLabel usernameLabel;
    private JPasswordField oldPasswordField;
    private JPasswordField newPasswordField;
    private JPasswordField repeatPasswordField;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JButton confirmButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}