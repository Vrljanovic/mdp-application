package gui.administrator;
import client.chat.AdministratorClient;
import client.chat.ChatClient;
import util.ExceptionLogger;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.*;
/*
 * Created by JFormDesigner on Fri Jun 07 15:56:15 CEST 2019
 */



/**
 * @author Nemanja Vrljanović
 */
class MonitorPanel extends JFrame {

    private String username;
    private AdministratorClient adminClient;
    private boolean notClosed ;
    static byte[] screenPicture = null;
    private AdministratorForm parent;

    MonitorPanel(AdministratorForm parent, AdministratorClient adminClient, String username) {
        initComponents();
        this.parent = parent;
        this.username = username;
        this.adminClient = adminClient;
        this.setTitle("Monitoring user: " + username);
        notClosed = true;
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                notClosed = false;
            }
        });
        monitor();
    }

    private void monitor(){
        new Thread(()-> {
            BufferedImage image;
            while(notClosed){
                java.util.List<String> loggedUsers = parent.getUsersFromJList();
                Thread.yield();
                if(!loggedUsers.contains(username)){
                    JOptionPane.showMessageDialog(this, "User logged out.", "Information", JOptionPane.INFORMATION_MESSAGE);
                    this.setVisible(false);
                    return;
                }
                adminClient.askForScreenShot(username);
                if(screenPicture != null) {
                    try {
                        image = ImageIO.read(new ByteArrayInputStream(screenPicture));
                        this.setSize(image.getWidth(), image.getHeight() + 35);
                        this.imageLabel.setSize(image.getWidth(), image.getHeight());
                        imageLabel.setIcon(new ImageIcon(image));
                    } catch (IOException ex) {
                        ExceptionLogger.log(Level.SEVERE, ex.getMessage());
                    }
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    ExceptionLogger.log(Level.SEVERE, e.getMessage());
                }
            }

        }).start();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        imageLabel = new JLabel();

        //======== this ========
        setMinimumSize(new Dimension(50, 50));
        setResizable(false);
        var contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(imageLabel);
        imageLabel.setBounds(new Rectangle(new Point(0, 0), imageLabel.getPreferredSize()));

        contentPane.setPreferredSize(new Dimension(1000, 600));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JLabel imageLabel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}