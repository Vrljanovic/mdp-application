/*
 * Created by JFormDesigner on Tue Jun 04 23:40:24 CEST 2019
 */

package gui.administrator;


import client.chat.AdministratorClient;
import util.ExceptionLogger;

import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javax.swing.*;

/**
 * @author Nemanja Vrljanović
 */
public class AdministratorForm extends JFrame {
    private AdministratorClient adminClient = new AdministratorClient();

    private static String ANNOUNCEMENT_MULTICAST_ADDRESS;
    private static int PORT ;


    static{
        try {
            Properties properties = new Properties();
            properties.load(new InputStreamReader(new FileInputStream("announcement.properties")));
            ANNOUNCEMENT_MULTICAST_ADDRESS = properties.getProperty("ANNOUNCEMENT_MULTICAST_ADDRESS");
            PORT = Integer.parseInt(properties.getProperty("ANNOUNCEMENT_MULTICAST_PORT"));
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    List<String> getUsersFromJList(){
        List<String> users = new LinkedList<>();
        for(int i = 0; i < activeUsersList.getModel().getSize(); ++i)
            users.add((String)activeUsersList.getModel().getElementAt(i));
        return users;
    }

    private AdministratorForm() {
        initComponents();
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                adminClient.closeConnection();
                System.exit(0);
            }
        });
        userActivity();
    }

    private void userActivity(){
        new Thread(()->{
            while(adminClient.isOpen()) {
                try {
                    String msg = adminClient.userActivity();
                    if(msg != null) {
                        if (msg.startsWith("HELLO")) {
                            String username = msg.split("#")[1];
                            List<String> users = getUsersFromJList();
                            Object selected = activeUsersList.getSelectedValue();
                            users.add(username);
                            activeUsersList.setListData(users.toArray());
                            activeUsersList.setSelectedValue(selected, true);

                        } else if (msg.startsWith("ACTIVEUSERS")) {
                            String[] data = msg.split("#");
                            if(data.length > 1) {
                                List<String> activeUsers = Arrays.asList(data).stream().filter(e -> !"ACTIVEUSERS".equals(e)).collect(Collectors.toList());
                                activeUsersList.setListData(activeUsers.toArray());
                            }
                        } else if(msg.startsWith("SCREENSHOT#")){
                            String encodedPicture = msg.split("#")[1];
                            MonitorPanel.screenPicture = Base64.getDecoder().decode(encodedPicture);
                        } else {
                            String username = msg.split("#")[1];
                            List<String> users = getUsersFromJList();
                            Object selected = activeUsersList.getSelectedValue();
                            users.remove(username);
                            activeUsersList.setListData(users.toArray());
                            activeUsersList.setSelectedValue(selected, true);
                        }
                    }
                    Thread.sleep(200);
                } catch (IOException | InterruptedException ex) {
                    ExceptionLogger.log(Level.SEVERE, ex.getMessage());
                }
            }

        }).start();
    }

    private void userStatistics(MouseEvent e) {
       new StatisticsForm(this).setVisible(true);
    }

    private void blockUnblockUser(MouseEvent e) {
        new BlockUnblockUserForm(this).setVisible(true);
    }

    private void addNewUser(MouseEvent e) {
        new AddUserForm().setVisible(true);
    }

    private void sendAnnouncement(MouseEvent e) {
        String announcement = announcementTextArea.getText();
        if (announcement == null) {
            JOptionPane.showMessageDialog(this, "Your announcement is empty.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            DatagramSocket socket = new DatagramSocket();
            byte[] message = announcementTextArea.getText().getBytes();
            DatagramPacket packet = new DatagramPacket(message, message.length, InetAddress.getByName(ANNOUNCEMENT_MULTICAST_ADDRESS), PORT);
            socket.send(packet);
            socket.close();
            announcementTextArea.setText("");
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    private void monitorUser(MouseEvent e) {
        String username = (String)activeUsersList.getSelectedValue();
        if(username == null)
            return;
        MonitorPanel panel = new MonitorPanel(this, adminClient, username);
        panel.setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        scrollPane1 = new JScrollPane();
        activeUsersList = new JList();
        userStatisticButton = new JButton();
        monitorButton = new JButton();
        blockUserButton = new JButton();
        addNewUserButton = new JButton();
        scrollPane2 = new JScrollPane();
        announcementTextArea = new JTextArea();
        sendAnnouncementButton = new JButton();
        label1 = new JLabel();
        label2 = new JLabel();

        //======== this ========
        setResizable(false);
        setTitle("MDP Application - Administrator");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(activeUsersList);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(20, 35, 200, 200);

        //---- userStatisticButton ----
        userStatisticButton.setText("User Statistics");
        userStatisticButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                userStatistics(e);
            }
        });
        contentPane.add(userStatisticButton);
        userStatisticButton.setBounds(20, 295, 200, 25);

        //---- monitorButton ----
        monitorButton.setText("Monitor");
        monitorButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                monitorUser(e);
            }
        });
        contentPane.add(monitorButton);
        monitorButton.setBounds(20, 255, 200, 25);

        //---- blockUserButton ----
        blockUserButton.setText("Block/Unblock User");
        blockUserButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                blockUnblockUser(e);
            }
        });
        contentPane.add(blockUserButton);
        blockUserButton.setBounds(20, 335, 200, 25);

        //---- addNewUserButton ----
        addNewUserButton.setText("Add New User");
        addNewUserButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addNewUser(e);
            }
        });
        contentPane.add(addNewUserButton);
        addNewUserButton.setBounds(275, 335, 200, 25);

        //======== scrollPane2 ========
        {

            //---- announcementTextArea ----
            announcementTextArea.setLineWrap(true);
            scrollPane2.setViewportView(announcementTextArea);
        }
        contentPane.add(scrollPane2);
        scrollPane2.setBounds(275, 35, 200, 200);

        //---- sendAnnouncementButton ----
        sendAnnouncementButton.setText("Send Announcement");
        sendAnnouncementButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendAnnouncement(e);
            }
        });
        contentPane.add(sendAnnouncementButton);
        sendAnnouncementButton.setBounds(275, 255, 200, 25);

        //---- label1 ----
        label1.setText("Active Users");
        contentPane.add(label1);
        label1.setBounds(20, 15, 175, label1.getPreferredSize().height);

        //---- label2 ----
        label2.setText("Write Announcement");
        contentPane.add(label2);
        label2.setBounds(275, 15, 200, label2.getPreferredSize().height);

        contentPane.setPreferredSize(new Dimension(500, 385));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JScrollPane scrollPane1;
    private JList activeUsersList;
    private JButton userStatisticButton;
    private JButton monitorButton;
    private JButton blockUserButton;
    private JButton addNewUserButton;
    private JScrollPane scrollPane2;
    private JTextArea announcementTextArea;
    private JButton sendAnnouncementButton;
    private JLabel label1;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public static void main(String[] args) {
        new AdministratorForm().setVisible(true);
    }
}