/*
 * Created by JFormDesigner on Thu Jun 06 12:49:32 CEST 2019
 */

package gui.administrator;

import java.awt.event.*;
import client.rest.RestClient;
import client.soap.AccountManagementServiceLocator;
import client.soap.User;
import util.ExceptionLogger;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.Set;
import java.util.logging.Level;
import javax.swing.*;
import javax.xml.rpc.ServiceException;

/**
 * @author Nemanja Vrljanović
 */
class BlockUnblockUserForm extends JDialog {

    BlockUnblockUserForm(Window owner){
        super(owner);
        initComponents();;
        this.setModal(true);
        fillComboBoxes();
    }

    private void fillComboBoxes(){
        ((DefaultComboBoxModel)activeUsersComboBox.getModel()).removeAllElements();
        ((DefaultComboBoxModel)blockedUsersComboBox.getModel()).removeAllElements();
        Set<String> blockedUsers = RestClient.getBlockedUsers();
        blockedUsers.forEach(u -> blockedUsersComboBox.addItem(u));
        Set<String> activeUsers = RestClient.getActiveUsers();
        activeUsers.forEach(u -> activeUsersComboBox.addItem(u));
    }

    private void unblockUser(MouseEvent e) {
        String username = (String)blockedUsersComboBox.getSelectedItem();
        if(username == null)
            return;
        try {
            if("OK".equals(new AccountManagementServiceLocator().getAccountManagement().activateUser(new User(null, username)))){
                JOptionPane.showMessageDialog(this, "User unblocked", "Information", JOptionPane.INFORMATION_MESSAGE);
                fillComboBoxes();
                return;
            }
        } catch (RemoteException | ServiceException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        JOptionPane.showMessageDialog(this, "Error while unblocking user", "Error", JOptionPane.ERROR_MESSAGE);
    }

    private void blockUser(MouseEvent e) {
        String username = (String)activeUsersComboBox.getSelectedItem();
        if(username == null)
            return;
        try{
            if("OK".equals(new AccountManagementServiceLocator().getAccountManagement().blockUser(new User(null, username)))){
                JOptionPane.showMessageDialog(this, "User blocked", "Information", JOptionPane.INFORMATION_MESSAGE);
                fillComboBoxes();
                return;
            }
        }catch(RemoteException | ServiceException ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        JOptionPane.showMessageDialog(this, "Error while blocking user", "Error", JOptionPane.ERROR_MESSAGE);

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        label1 = new JLabel();
        blockedUsersComboBox = new JComboBox();
        unblockUsersButton = new JButton();
        activeUsersComboBox = new JComboBox();
        blockUserButton = new JButton();
        label2 = new JLabel();

        //======== this ========
        setTitle("MDP Admin - Block/Unblock User");
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("Choose User to Unblock");
        contentPane.add(label1);
        label1.setBounds(20, 20, 150, 20);
        contentPane.add(blockedUsersComboBox);
        blockedUsersComboBox.setBounds(20, 40, 160, 30);

        //---- unblockUsersButton ----
        unblockUsersButton.setText("Unblock User");
        unblockUsersButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                unblockUser(e);
            }
        });
        contentPane.add(unblockUsersButton);
        unblockUsersButton.setBounds(20, 95, 160, 35);
        contentPane.add(activeUsersComboBox);
        activeUsersComboBox.setBounds(230, 40, 160, 30);

        //---- blockUserButton ----
        blockUserButton.setText("Block User");
        blockUserButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                blockUser(e);
            }
        });
        contentPane.add(blockUserButton);
        blockUserButton.setBounds(230, 95, 160, 35);

        //---- label2 ----
        label2.setText("Choose User to Block");
        contentPane.add(label2);
        label2.setBounds(230, 25, 160, label2.getPreferredSize().height);

        contentPane.setPreferredSize(new Dimension(415, 190));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JLabel label1;
    private JComboBox blockedUsersComboBox;
    private JButton unblockUsersButton;
    private JComboBox activeUsersComboBox;
    private JButton blockUserButton;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}