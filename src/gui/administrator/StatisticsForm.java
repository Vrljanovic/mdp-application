/*
 * Created by JFormDesigner on Thu Jun 06 13:59:40 CEST 2019
 */

package gui.administrator;

 import client.rest.RestClient;
 import model.User;

 import java.awt.*;
 import java.awt.event.*;
 import java.util.Set;
 import javax.swing.*;

/**
 * @author Nemanja Vrljanović
 */
class StatisticsForm extends JDialog {

    StatisticsForm(Window owner) {
        super(owner);
        initComponents();
        Set<String> allUsers = RestClient.getAllUsers();
        allUsersList.setListData(allUsers.toArray());
        this.setModal(true);
    }

    private void getStatistics(MouseEvent e) {
        String username = (String)allUsersList.getSelectedValue();
        if(username == null) {
            statisticArea.setText("");
            return;
        }
        statisticArea.setText("");
        usernameLabel.setText("Statistics for " + username);
        statisticArea.append(RestClient.getStatistics(new User(username, null)));
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        scrollPane1 = new JScrollPane();
        allUsersList = new JList();
        label1 = new JLabel();
        scrollPane2 = new JScrollPane();
        statisticArea = new JTextArea();
        usernameLabel = new JLabel();

        //======== this ========
        setTitle("MDP Admin - User Statistics");
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {

            //---- allUsersList ----
            allUsersList.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    getStatistics(e);
                }
            });
            scrollPane1.setViewportView(allUsersList);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(10, 30, 185, 290);

        //---- label1 ----
        label1.setText("Choose User");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 2f));
        contentPane.add(label1);
        label1.setBounds(10, 5, 185, 18);

        //======== scrollPane2 ========
        {

            //---- statisticArea ----
            statisticArea.setEditable(false);
            scrollPane2.setViewportView(statisticArea);
        }
        contentPane.add(scrollPane2);
        scrollPane2.setBounds(210, 30, 375, 290);

        //---- usernameLabel ----
        usernameLabel.setText("Statistics for ");
        usernameLabel.setFont(usernameLabel.getFont().deriveFont(usernameLabel.getFont().getSize() + 2f));
        contentPane.add(usernameLabel);
        usernameLabel.setBounds(210, 5, 375, 20);

        contentPane.setPreferredSize(new Dimension(600, 350));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JScrollPane scrollPane1;
    private JList allUsersList;
    private JLabel label1;
    private JScrollPane scrollPane2;
    private JTextArea statisticArea;
    private JLabel usernameLabel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}