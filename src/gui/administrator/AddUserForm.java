/*
 * Created by JFormDesigner on Wed Jun 05 10:47:57 CEST 2019
 */

package gui.administrator;

import client.soap.AccountManagementServiceLocator;
import client.soap.User;
import util.ExceptionLogger;

import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import java.util.logging.Level;
import javax.swing.*;
import javax.xml.rpc.ServiceException;

/**
 * @author Nemanja Vrljanović
 */
class AddUserForm extends JDialog {

    AddUserForm() {
        initComponents();
        this.setModal(true);
    }

    private void addUser(MouseEvent e) {
        String username = usernameTextfield.getText();
        String newPassword = new String(passwordField.getPassword());
        String repeatedPassword = new String(repeatedPasswordFiled.getPassword());
        if(username == null || newPassword.equals("") || repeatedPassword.equals("")){
            JOptionPane.showMessageDialog(this, "Insert user data", "Error", JOptionPane.ERROR_MESSAGE);
        }else {
            if(username.contains("#")){
                JOptionPane.showMessageDialog(this, "Username must not contain character '#'", "Error", JOptionPane.ERROR_MESSAGE);
                clearFields();
                return;
            }
            if (newPassword.equals(repeatedPassword)) {
                try {
                    if ("OK".equals(new AccountManagementServiceLocator().getAccountManagement().addUser(new User(newPassword, username)))) {
                        JOptionPane.showMessageDialog(this, "User added successfully", "Error", JOptionPane.ERROR_MESSAGE);
                        this.setVisible(false);
                        return;
                    }
                } catch (RemoteException | ServiceException ex) {
                    ExceptionLogger.log(Level.SEVERE, ex.getMessage());
                }
                JOptionPane.showMessageDialog(this, "Error while adding new user", "Error", JOptionPane.ERROR_MESSAGE);
            } else
                JOptionPane.showMessageDialog(this, "Passwords don't match", "Error", JOptionPane.ERROR_MESSAGE);
        }
        clearFields();
    }

    private void clearFields(){
        this.passwordField.setText("");
        this.repeatedPasswordFiled.setText("");
        this.usernameTextfield.setText("");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
        usernameTextfield = new JTextField();
        passwordField = new JPasswordField();
        repeatedPasswordFiled = new JPasswordField();
        confirmButton = new JButton();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();

        //======== this ========
        setTitle("MDP Admin - Add User");
        setResizable(false);
        var contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(usernameTextfield);
        usernameTextfield.setBounds(75, 40, 150, 30);
        contentPane.add(passwordField);
        passwordField.setBounds(75, 90, 150, 30);
        contentPane.add(repeatedPasswordFiled);
        repeatedPasswordFiled.setBounds(75, 140, 150, 30);

        //---- confirmButton ----
        confirmButton.setText("Confirm");
        confirmButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addUser(e);
            }
        });
        contentPane.add(confirmButton);
        confirmButton.setBounds(95, 190, 110, 30);

        //---- label1 ----
        label1.setText("Username");
        contentPane.add(label1);
        label1.setBounds(75, 20, 150, 18);

        //---- label2 ----
        label2.setText("Password");
        contentPane.add(label2);
        label2.setBounds(75, 70, 150, 18);

        //---- label3 ----
        label3.setText("Repeat Password");
        contentPane.add(label3);
        label3.setBounds(75, 120, 150, 20);

        contentPane.setPreferredSize(new Dimension(300, 300));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Nemanja Vrljanović
    private JTextField usernameTextfield;
    private JPasswordField passwordField;
    private JPasswordField repeatedPasswordFiled;
    private JButton confirmButton;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}