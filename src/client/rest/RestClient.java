package client.rest;

import model.User;
import util.ExceptionLogger;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

public class RestClient {

    private static WebTarget restTarget;

    static{
        Properties properties = new Properties();
        try {
            properties.load(new InputStreamReader(new FileInputStream("webserver.properties")));
            String webServerAddress = properties.getProperty("WEB_SERVER_ADDRESS");
            int webServerPort = Integer.parseInt(properties.getProperty("WEB_SERVER_PORT"));
            String restBasePath = properties.getProperty("REST_BASE_PATH");
            restTarget = ClientBuilder.newClient().target("http://" + webServerAddress + ":" + webServerPort + "/" + restBasePath);
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public static boolean login(User user){
        return callHTTPostMethod(user, "login");
    }

    public static boolean logout(User user){
        return callHTTPostMethod(user, "logout");
    }

    public static boolean changePassword(User user){
        return callHTTPostMethod(user, "changepass");
    }

    public static String getStatistics(User user){
       return restTarget.path("/statistic").path("/" + user.getUsername()).request(MediaType.TEXT_PLAIN_TYPE).get(String.class);
    }

    public static Set<String> getAllUsers(){
        return restTarget.path("/users").request(MediaType.APPLICATION_JSON_TYPE).get(Set.class);
    }

    public static Set<String> getBlockedUsers(){
        return restTarget.path("/users").path("/blocked").request(MediaType.APPLICATION_JSON_TYPE).get(Set.class);
    }

    public static Set<String> getActiveUsers(){
        return restTarget.path("/users").path("/active").request(MediaType.APPLICATION_JSON_TYPE).get(Set.class);
    }

    private static boolean callHTTPostMethod(User user, String method){
        return "OK".equals(restTarget.path("/" + method).request(MediaType.TEXT_PLAIN_TYPE).post(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE), String.class));
    }
}