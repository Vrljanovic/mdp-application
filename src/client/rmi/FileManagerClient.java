package client.rmi;

import server.rmi.FileManagerInterface;
import util.ExceptionLogger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;
import java.util.logging.Level;

public class FileManagerClient {

    private static FileManagerInterface fileManager;

    static {
        System.setProperty("java.security.policy", "security.policy");
        if (System.getSecurityManager() == null)
            System.setSecurityManager(new SecurityManager());

        try {
            Properties properties = new Properties();
            properties.load(new InputStreamReader(new FileInputStream("rmi.properties")));
            int rmiServicePort = Integer.parseInt(properties.getProperty("RMI_SERVER_PORT"));
            String rmiServerAddress = properties.getProperty("RMI_SERVER_ADDRESS");
            String rmiServiceName = properties.getProperty("RMI_SERVICE_NAME");
            Registry registry = LocateRegistry.getRegistry(rmiServerAddress, rmiServicePort);
            fileManager = (FileManagerInterface) registry.lookup(rmiServiceName);
        } catch (NotBoundException | IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());;
        }
    }

    public static String[] getAllFiles(String username) throws RemoteException {
        return fileManager.reviewFiles(username);
    }

    public static void downloadFile(String username, String filename, String sender, String destinationDirectory) throws IOException {
        byte[] fileBytes = fileManager.downloadFile(username, sender, filename);

        DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(destinationDirectory + File.separator + filename));
        outputStream.write(fileBytes);
        outputStream.close();
    }

    public static void sendFile(File file, String sender, String destinationUser) throws IOException {
        fileManager.uploadFile(file.getName(), Files.readAllBytes(Path.of(file.getAbsolutePath())), sender, destinationUser);
    }
}