package client.chat;

import util.ExceptionLogger;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.util.Properties;
import java.util.logging.Level;

public class AdministratorClient {

    private SSLSocket socket;
    private BufferedReader in;
    private PrintWriter out;

    private static String PATH_TO_TRUSTSTORE;
    private static String TRUSTSTORE_PASSWORD;
    private static String CHAT_SERVER_ADDRESS;
    private static int CHAT_SERVER_PORT;

    static {
        Properties properties = new Properties();
        try {
            properties.load(new InputStreamReader(new FileInputStream("chatclient.properties")));
            PATH_TO_TRUSTSTORE = properties.getProperty("CLIENT_TRUSTSTORE_PATH");
            TRUSTSTORE_PASSWORD = properties.getProperty("CLIENT_TRUSTSTORE_PASSWORD");
            CHAT_SERVER_ADDRESS = properties.getProperty("CHAT_SERVER_ADDRESS");
            CHAT_SERVER_PORT = Integer.parseInt(properties.getProperty("CHAT_SERVER_PORT"));
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public AdministratorClient() {
        System.setProperty("javax.net.ssl.trustStore", PATH_TO_TRUSTSTORE);
        System.setProperty("javax.net.ssl.trustStorePassword", TRUSTSTORE_PASSWORD);

        try {
            socket = (SSLSocket) (SSLSocketFactory.getDefault()).createSocket(CHAT_SERVER_ADDRESS, CHAT_SERVER_PORT);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            out.println("ADMINHELLO");
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    private void bye() {
        out.println("ADMINBYE");
        try {
            socket.close();
            in.close();
            out.close();
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public void askForScreenShot(String username) {
        out.println("MONITOR#" + username);
    }

    public String userActivity() throws IOException {
        return in.readLine();
    }

    public void closeConnection(){
        bye();
        out.close();
        try {
            in.close();
            socket.close();
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }

    }

    public boolean isOpen(){
        return !socket.isClosed();
    }
}