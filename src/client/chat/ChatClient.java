package client.chat;

import model.User;
import org.imgscalr.Scalr;
import util.ExceptionLogger;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.logging.Level;

public class ChatClient {

    private User user;
    private SSLSocket socket;
    private BufferedReader in;
    private PrintWriter out;

    private static String CHAT_SERVER_ADDRESS;
    private static int CHAT_SERVER_PORT;
    private static String PATH_TO_TRUSTSTORE;
    private static String TRUSTSTORE_PASSWORD;
    private static final int IMAGE_SIZE = 1000;
    private Set<String> loggedUsers = new HashSet<>();

    static{
        Properties properties = new Properties();
        try {
            properties.load(new InputStreamReader(new FileInputStream("chatclient.properties")));
            CHAT_SERVER_ADDRESS = properties.getProperty("CHAT_SERVER_ADDRESS");
            CHAT_SERVER_PORT = Integer.parseInt(properties.getProperty("CHAT_SERVER_PORT"));
            PATH_TO_TRUSTSTORE = properties.getProperty("CLIENT_TRUSTSTORE_PATH");
            TRUSTSTORE_PASSWORD = properties.getProperty("CLIENT_TRUSTSTORE_PASSWORD");
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public ChatClient(User user) {
        System.setProperty("javax.net.ssl.trustStore", PATH_TO_TRUSTSTORE);
        System.setProperty("javax.net.ssl.trustStorePassword", TRUSTSTORE_PASSWORD);

        this.user = user;
        try{
            SSLSocketFactory sslSocketFactory = (SSLSocketFactory)SSLSocketFactory.getDefault();
            socket = (SSLSocket) sslSocketFactory.createSocket(CHAT_SERVER_ADDRESS, CHAT_SERVER_PORT);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        }catch(IOException ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        sayHello();
        askForLoggedUsers();
    }

    public void sendMessage(String receiver, String message) {
        out.println("MSG#" + user.getUsername() + "#" + receiver + "#" + message);
    }

    public void sendGroupMessage(String content){
        out.println("GROUPMSG#" + user.getUsername() + "#" + content);
    }

    public String[] receiveMessage() throws IOException {
        String message = in.readLine();
        if (message == null)
            return null;
        String[] data = message.split("#");
        if("TAKESCREENSHOT".equals(message)){
            sendScreenShot();
            return null;
        } else if("MSGFROM".equals(data[0])){
            String[] msgInfo = new String[2];
            msgInfo[0] = data[1];
            msgInfo[1] = data[2];
            return msgInfo;
        } else if("GROUPMSG".equals(data[0])){
            String[] msgInfo = new String[3];
            msgInfo[0] = "GROUPMSG";
            msgInfo[1] = data[1];
            msgInfo[2] = data[2];
            return msgInfo;
        } else if("ACTIVEUSERS".equals(data[0]) && data.length > 1) {
            loggedUsers.clear();
            loggedUsers.addAll(Arrays.asList(data).subList(1, data.length));
        }
        return null;
    }

    public void askForLoggedUsers(){
        out.println("ACTIVEUSERS");
    }

    private void sendScreenShot(){
        try {
            Robot robot = new Robot();
            BufferedImage screenshot = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            BufferedImage resized = Scalr.resize(screenshot, IMAGE_SIZE, BufferedImage.SCALE_FAST);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(resized, "jpg", byteArrayOutputStream);
            byteArrayOutputStream.flush();
            byte[] picture = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            out.println("MYSCREENSHOT#" + Base64.getEncoder().encodeToString(picture));
        } catch (AWTException | IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    private void sayHello(){
        out.println("HELLO#" + user.getUsername());
    }

    private void sayGoodBye(){
        out.println("GOODBYE#" + user.getUsername());
    }

    public void closeConnection(){
        sayGoodBye();
        out.close();
        try {
            in.close();
            socket.close();
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());;
        }
    }

    public User getUser(){
        return user;
    }

    public Set<String> getCurrentlyLoggedUsers(){
        return loggedUsers;
    }
}