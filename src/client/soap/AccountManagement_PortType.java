/**
 * AccountManagement_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package client.soap;

public interface AccountManagement_PortType extends java.rmi.Remote {
    public java.lang.String blockUser(client.soap.User user) throws java.rmi.RemoteException;
    public java.lang.String addUser(client.soap.User user) throws java.rmi.RemoteException;
    public java.lang.String activateUser(client.soap.User user) throws java.rmi.RemoteException;
}
