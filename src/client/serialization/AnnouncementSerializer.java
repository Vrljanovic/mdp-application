package client.serialization;

import com.esotericsoftware.kryo.Kryo;
import com.google.gson.Gson;
import com.esotericsoftware.kryo.io.Output;
import org.jboss.netty.handler.codec.serialization.ObjectEncoderOutputStream;
import util.ExceptionLogger;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;

public class AnnouncementSerializer {

    private static int SERIALIZATION_TYPE;
    private static Properties properties;

    static {
        properties =  new Properties();
        try {
            properties.load(new FileInputStream("serialization.properties"));
            SERIALIZATION_TYPE = Integer.parseInt(properties.getProperty("SERIALIZATION_TYPE"));
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public void serializeAnnouncement(String announcement){
        try {
            if(SERIALIZATION_TYPE == 0)
                javaSerialize(announcement);
            else if(SERIALIZATION_TYPE == 1)
                gsonSerialize(announcement);
            else if(SERIALIZATION_TYPE == 2)
                kryoSerialize(announcement);
            else {
                nettySerialize(announcement);
                SERIALIZATION_TYPE = 0;
                return;
            }
            ++SERIALIZATION_TYPE;
            properties.setProperty("SERIALIZATION_TYPE", SERIALIZATION_TYPE + "");
            properties.store(new FileOutputStream("serialization.properties"), "");
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());;
        }
    }

    private static void javaSerialize(String announcement) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("Announcements" + File.separator + "announcement_" + System.currentTimeMillis() + ".ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(announcement);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    private static void gsonSerialize(String announcement) throws IOException {
        Gson gson = new Gson();
        FileWriter fileWriter = new FileWriter("Announcements" + File.separator + "announcement_" + System.currentTimeMillis() + ".json", true);
        gson.toJson(announcement, fileWriter);
        fileWriter.close();
    }

    private static void kryoSerialize(String announcement) throws IOException {
        Kryo kryo = new Kryo();
        kryo.register(String.class);
        FileOutputStream fileOutputStream = new FileOutputStream("Announcements" + File.separator + "announcement_" + System.currentTimeMillis() + ".kryo", true);
        Output output = new Output(fileOutputStream);
        kryo.writeClassAndObject(output, announcement);
        output.close();
        fileOutputStream.close();
    }

    private static void nettySerialize(String announcement) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("Announcements" + File.separator + "announcement_" + System.currentTimeMillis() + ".netty", true);
        ObjectEncoderOutputStream encoderOutputStream = new ObjectEncoderOutputStream(fileOutputStream);
        encoderOutputStream.writeObject(announcement);
        encoderOutputStream.flush();
        encoderOutputStream.close();
        fileOutputStream.close();

    }
}