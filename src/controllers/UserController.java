package controllers;

import model.User;
import redis.clients.jedis.Jedis;
import util.ExceptionLogger;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

public class UserController {

    public static boolean login(User user){ ;
        Jedis jedis = new Jedis();
        boolean result = false;
        try{
            if(user.getPassword().equals(jedis.lrange(user.getUsername(), 0, 1).get(0)) && "1".equals(jedis.lrange(user.getUsername(), 1, 1).get(0))) {
                jedis.rpush(user.getUsername(), System.currentTimeMillis() + "");
                result = true;
            }
        }
        catch(Exception ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        jedis.close();
        return result;
    }

    public static boolean logout(User user){
        Jedis redisDb = new Jedis();
        redisDb.rpush(user.getUsername(), System.currentTimeMillis() + "");
        redisDb.close();
        return true;
    }

    public static boolean changePassword(User user){
        Jedis redisDb = new Jedis();
        boolean result = false;
        try {
            result = ("OK".equals(redisDb.lset(user.getUsername(), 0, user.getPassword())));
        }catch(Exception ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        redisDb.close();
        return result;
    }

    public static boolean addNewUser(User newUser){
        boolean result = true;
        Jedis redisDb = new Jedis();
        try {
            if (redisDb.exists(newUser.getUsername()))
                result = false;
            redisDb.rpush(newUser.getUsername(), newUser.getPassword(), 1 + "");
        }catch (Exception ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
            result =  false;
        }
        redisDb.close();
        return result;
    }

    public static boolean blockUser(User user) { ;
        Jedis redisDb = new Jedis();
        boolean result = false;
        try {
            result = ("OK".equals(redisDb.lset(user.getUsername(), 1, 0 + "")));
        } catch (Exception ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        redisDb.close();
        return result;
    }

    public static boolean activateUser(User blockedUser){ ;
        Jedis redisDb = new Jedis();
        boolean result = false;
        try{
            result = ("OK".equals(redisDb.lset(blockedUser.getUsername(), 1, 1 + "")));
        }catch(Exception ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        redisDb.close();
        return result;
    }

    public static Set<String> getAllUsers(){
        Jedis redisDb = new Jedis();
        Set<String> allUsers = redisDb.keys("*");
        redisDb.close();
        return allUsers;
    }

    public static Set<String> getBlockedUsers(){
        Set<String> allUsers = getAllUsers();
        Set<String> blockedUsers = new HashSet<>();
        Jedis redisDb = new Jedis();
        allUsers.forEach(u -> {
            if("0".equals(redisDb.lrange(u, 1, 1).get(0)))
                blockedUsers.add(u);
        });
        redisDb.close();
        return blockedUsers;
    }

    public static Set<String> getActiveUsers(){
        Set<String> allUsers = getAllUsers();
        Set<String> activeUsers = new HashSet<>();
        Jedis redisDb = new Jedis();
        allUsers.forEach(u ->{
            if ("1".equals(redisDb.lrange(u, 1, 1).get(0)))
                activeUsers.add(u);
        });
        redisDb.close();
        return activeUsers;
    }
}