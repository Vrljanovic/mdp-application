package model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Active {

    private User user;
    private long loginTime;
    private long logoutTime;
    private long activeTime;

    public Active(User user, Long loginTime, Long logoutTime){
        this.loginTime = loginTime;
        this.logoutTime = logoutTime;
        this.activeTime = this.logoutTime - this.loginTime;
        this.user = user;
    }

    public User getUser(){
        return  user;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss");
        Date login = new Date(loginTime);
        Date logout = new Date(logoutTime);

        double activeInMinutes = (double)(activeTime / 1_000);
        return  "Login time: " + simpleDateFormat.format(login) + "\n" +
                "Logout time: " + simpleDateFormat.format(logout) + "\n" +
                "Active time: " + activeInMinutes + " seconds\n";
    }
}