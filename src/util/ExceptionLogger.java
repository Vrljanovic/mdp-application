package util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExceptionLogger {
    private static Logger logger = Logger.getLogger("error");

    static {
        try {
            logger.addHandler(new FileHandler("error.log", true));
            logger.setLevel(Level.INFO);
        } catch (IOException  ex) {
            ex.printStackTrace();
        }
    }

    public static void log(Level level, String message) {
        logger.log(level, message);
    }

}
