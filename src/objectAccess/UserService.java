package objectAccess;

import model.Active;
import model.User;
import redis.clients.jedis.Jedis;
import util.ExceptionLogger;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

public class UserService {

    private User user;

    public UserService(User user){
        this.user = user;
    }

    private List<String> readAllData(){
        Jedis redisDb = new Jedis();
        List<String> allData = redisDb.lrange(user.getUsername(), 2, redisDb.llen(user.getUsername()));
        redisDb.close();
        return allData;
    }

    public List<Active> getActivityData(){
        List<Active> allLoginData = new LinkedList<>();

        List<String> all = readAllData();
        try {
            for(int i = 0; i < all.size(); i += 2){
                Long login = Long.parseLong(all.get(i));
                Long logout = Long.parseLong(all.get(i + 1));
                Active active = new Active(user, login, logout);
                allLoginData.add(active);
            }
        }catch(Exception ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        return allLoginData;
    }
}