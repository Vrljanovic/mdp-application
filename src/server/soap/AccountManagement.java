package server.soap;

import controllers.UserController;
import model.User;

public class AccountManagement{

    public String addUser(User user) {
        if (UserController.addNewUser(user))
            return "OK";
        return "NOK";
    }

    public String blockUser(User user){
        if(UserController.blockUser(user))
            return "OK";
        return "NOK";
    }

    public String activateUser(User user){
        if(UserController.activateUser(user))
            return "OK";
        return "NOK";
    }
}