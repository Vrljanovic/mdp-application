package server.rmi;

import util.ExceptionLogger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.stream.Stream;

public class FileManagerServer implements FileManagerInterface {

    @Override
    public byte[] downloadFile(String username, String sendersUsername, String fileName) throws RemoteException {
        try {
            File fileToDownload = new File ("UploadedFiles" + File.separator + username + File.separator + sendersUsername + "_" + fileName);
            if(fileToDownload.exists())
            return Files.readAllBytes(fileToDownload.toPath());
        }catch(Exception ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        return null;
    }

    @Override
    public void uploadFile(String fileName, byte[] fileBytes, String senderUsername, String receiverUsername) throws RemoteException {
        try{
            File file = new File("UploadedFiles" + File.separator + receiverUsername);
            if(!file.isDirectory())
                file.mkdir();
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(new File(file.getCanonicalPath() +  File.separator + senderUsername + "_" + fileName)));
            dataOutputStream.write(fileBytes);
            dataOutputStream.close();
        }catch(IOException ex){
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());;
        }
    }

    @Override
    public String[] reviewFiles(String username) throws RemoteException {
        try {
            Stream<Path> stream = Files.list(Path.of("UploadedFiles" + File.separator + username));
            List<String> files = new LinkedList<>();
            stream.forEach(e -> files.add(e.toFile().getName()));
            String[] toReturn = new String[files.size()];
            files.toArray(toReturn);
            return toReturn;
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
            return null;
        }
    }

    public static void main(String[] args) {
        System.setProperty("java.security.policy", "security.policy");
        if(System.getSecurityManager() == null)
            System.setSecurityManager(new SecurityManager());

        Properties properties = new Properties();
        FileManagerServer fileManagerServer = new FileManagerServer();
        try {
            properties.load(new InputStreamReader(new FileInputStream("rmi.properties")));
            int rmiServerPort = Integer.parseInt(properties.getProperty("RMI_SERVER_PORT"));
            String rmiServiceName = properties.getProperty("RMI_SERVICE_NAME");
            FileManagerInterface stub = (FileManagerInterface) UnicastRemoteObject.exportObject(fileManagerServer, rmiServerPort);
            LocateRegistry.createRegistry(1099);
            LocateRegistry.getRegistry().rebind(rmiServiceName,   stub);
            System.out.println("File Manager Server Ready");
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }
}