package server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FileManagerInterface extends Remote {

    void uploadFile(String fileName, byte[] fileBytes, String sendersUsername, String receiverUsername) throws RemoteException;

    byte[] downloadFile(String username, String sendersUsername, String fileName) throws RemoteException;

    String[] reviewFiles(String username)throws RemoteException;
}