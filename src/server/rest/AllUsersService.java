package server.rest;

import controllers.UserController;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/users")
public class AllUsersService {

    @GET
    @Produces (MediaType.APPLICATION_JSON)
    public Set<String> getAllUsers(){
       return UserController.getAllUsers();
    }

    @GET
    @Path("/blocked")
    @Produces (MediaType.APPLICATION_JSON)
    public Set<String> getBlockedUsers(){
        return UserController.getBlockedUsers();
    }

    @GET
    @Path("/active")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<String> getActiveUsers(){
        return UserController.getActiveUsers();
    }
}