package server.rest;

import controllers.UserController;
import model.User;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/login")
public class LoginService {

    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.TEXT_PLAIN)
    public String login(User user){
        if(UserController.login(user)) {
            return "OK";
        }
        return "NOK";
    }
}