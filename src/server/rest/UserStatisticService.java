package server.rest;

import model.Active;
import model.User;
import objectAccess.UserService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/statistic")
public class UserStatisticService {

    @GET
    @Path("/{username}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getUserStatistic(@PathParam("username") String username){
        UserService userService = new UserService(new User(username, ""));
        List<Active> userActivity = userService.getActivityData();
        String result = "";

        for(Active a : userActivity)
            result += a + "\n";
        return result;
    }
}