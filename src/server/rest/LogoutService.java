package server.rest;

import controllers.UserController;
import model.User;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/logout")
public class LogoutService {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces (MediaType.TEXT_PLAIN)
    public String logout(User user){
        if(UserController.logout(user))
            return "OK";
        return "NOK";
    }
}