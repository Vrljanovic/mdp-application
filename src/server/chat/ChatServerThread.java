package server.chat;

import util.ExceptionLogger;

import javax.net.ssl.SSLSocket;
import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class ChatServerThread extends Thread{

    private SSLSocket socket;
    private BufferedReader in;

    ChatServerThread(Socket socket){
        this.socket = (SSLSocket)socket;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
        start();
    }

    @Override
    public void run(){
        String message = "";
        while(!socket.isClosed()){
            try {
                message = in.readLine();
                if(message != null) {
                    if ("ADMINHELLO".equals(message)) {
                        ChatServer.adminSocket = socket;
                        Set<String> users = ChatServer.loggedUsers.keySet();
                        String messageToAdmin = "ACTIVEUSERS#";
                        for (String username : users)
                            messageToAdmin += username + "#";
                        messageToAdmin = messageToAdmin.substring(0, messageToAdmin.length() - 1);
                        if(!ChatServer.adminSocket.isClosed())
                            new PrintWriter(new OutputStreamWriter(ChatServer.adminSocket.getOutputStream()), true).println(messageToAdmin);
                    } else if ("ADMINBYE".equals(message)) {
                        ChatServer.adminSocket.close();
                        break;
                    } else if (message.startsWith("MONITOR#")) {
                        String username = message.split("#")[1];
                        Socket sck = ChatServer.getSocketByUser(username);
                        if(sck != null)
                            new PrintWriter(new OutputStreamWriter(sck.getOutputStream()), true).println("TAKESCREENSHOT");
                    } else if(message.startsWith("MYSCREENSHOT#")){
                        String pictureInfo = message.split("#")[1];
                        String msg = "SCREENSHOT#" + pictureInfo;
                        if(ChatServer.adminSocket != null && !ChatServer.adminSocket.isClosed())
                            new PrintWriter(new OutputStreamWriter(ChatServer.adminSocket.getOutputStream()), true).println(msg);
                    } else if (message.startsWith("HELLO#")) {
                        String username = message.split("#")[1];
                        ChatServer.newLogin(username, socket);
                        if (ChatServer.adminSocket != null && !ChatServer.adminSocket.isClosed())
                            new PrintWriter(new OutputStreamWriter(ChatServer.adminSocket.getOutputStream()), true).println(message);
                    } else if (message.startsWith("GOODBYE#")) {
                        String username = message.split("#")[1];
                        ChatServer.newLogout(username);
                        socket.close();
                        if (ChatServer.adminSocket != null && !ChatServer.adminSocket.isClosed())
                            new PrintWriter(new OutputStreamWriter(ChatServer.adminSocket.getOutputStream()), true).println(message);
                        break;
                    } else if(message.startsWith("ISACTIVE#")){
                        String username = message.split("#")[1];
                        String active = "";
                        if(ChatServer.loggedUsers.containsKey(username))
                            active = "ACTIVE";
                        else
                            active = "NOTACTIVE";
                        new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true).println("USER#" + username + "#" + active);

                    } else if (message.startsWith("MSG#")) {
                        String[] data = message.split("#");
                        String sender = data[1];
                        String receiver = data[2];
                        String messageContent = data[3];
                        Socket socket = ChatServer.getSocketByUser(receiver);
                        if (socket != null)
                            new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true).println(
                                    "MSGFROM#" + sender + "#" + messageContent);
                    } else if (message.startsWith("GROUPMSG#")) {
                        String sender = message.split("#")[1];
                        List<String> users = ChatServer.loggedUsers.keySet().stream().filter(u -> !u.equals(sender)).collect(Collectors.toList());
                        for (String u : users) {
                            Socket sck = ChatServer.getSocketByUser(u);
                            if(sck != null)
                                 new PrintWriter(new OutputStreamWriter(sck.getOutputStream()), true).println(message);
                        }
                    } else if("ACTIVEUSERS".equals(message)){
                        String response = "ACTIVEUSERS#";
                        Set<String> users = ChatServer.loggedUsers.keySet();
                        for (String username : users)
                            response += username + "#";
                        response = response.substring(0, response.length() -1);
                        new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true).println(response);
                    }
                    yield();
                }

            } catch (IOException ex) {
                ExceptionLogger.log(Level.SEVERE, ex.getMessage());
                break;
            }

        }
    }
}