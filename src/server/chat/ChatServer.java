package server.chat;

import util.ExceptionLogger;

import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

public class ChatServer{

    static Map<String, Socket> loggedUsers = new HashMap<>();
    static Socket adminSocket;
    private static String KEY_STORE_PATH;
    private static String KEY_STORE_PASSWORD;
    private static int CHAT_SERVER_PORT;

    static{
        Properties properties = new Properties();
        try {
            properties.load(new InputStreamReader(new FileInputStream("chatserver.properties")));
            KEY_STORE_PATH = properties.getProperty("CHAT_SERVER_KEYSTORE_PATH");
            KEY_STORE_PASSWORD = properties.getProperty("CHAT_SERVER_KEYSTORE_PASSWORD");
            CHAT_SERVER_PORT = Integer.parseInt(properties.getProperty("CHAT_SERVER_PORT"));
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public static void main(String... args){
        System.setProperty("javax.net.ssl.keyStore", KEY_STORE_PATH);
        System.setProperty("javax.net.ssl.keyStorePassword", KEY_STORE_PASSWORD);
        try {
            SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
            ServerSocket serverSocket = ssf.createServerSocket(CHAT_SERVER_PORT);
            System.out.println("Chat Server Ready");
            while(true){
                SSLSocket socket = (SSLSocket) serverSocket.accept();
                new ChatServerThread(socket);
            }
        } catch (IOException ex) {
            ExceptionLogger.log(Level.SEVERE, ex.getMessage());
        }
    }

    static void newLogin(String username, Socket socket){
        loggedUsers.put(username, socket);
    }

    static Socket getSocketByUser(String username){
        return loggedUsers.get(username);
    }

    static void newLogout(String username){
        loggedUsers.remove(username);
    }
}